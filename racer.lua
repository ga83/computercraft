width, height = term.getSize()
horizon = height / 2

track = {}
cameraposition = 1

function draw()
 term.setBackgroundColor(colors.black)
 term.clear()
 local c = height 
 local xcentre = width / 2
 local coffset = 0
 local nextseg = 0

 local sky = 0
 while sky <= horizon do
  paintutils.drawLine(1,sky,width,sky,colors.white)
  sky = sky + 1
 end
 
 local grass = horizon + 1
 while grass <= height do
  paintutils.drawLine(1,grass,width,grass,colors.green)
  grass = grass + 1
 end
            
 while c > horizon do
  coffset = coffset + track[cameraposition + nextseg]
  rwidth = ((c - horizon) / (height - horizon)) * width
  swidth = (c - horizon) / 3
  lwidth = (c - horizon) / 12
  segheight = (c - horizon) / 10

  xcentre = xcentre + (coffset / 2)

  x1 = xcentre - (rwidth / 2)
  x2 = xcentre + (rwidth / 2) 

  while segheight > 0 do
   if (cameraposition + nextseg) % 4 < 2 then
    paintutils.drawLine(1,c,width,c,colors.lime)
   end   
   paintutils.drawLine(x1,c,x2,c,colors.black)
   if (cameraposition + nextseg) % 10 < 2 then
    paintutils.drawLine(xcentre - lwidth,c,xcentre + lwidth,c,1)
   end
   segheight = segheight - 1
   c = c - 1
  end
  nextseg = nextseg + 1  
      
 end



end

function createtrack()
 local c = 0
 while c < 100000 do
  local curve = math.random() * 1.4 - 0.7
  local length = math.random(2,20)
  local x = 0
  while x < length do
   table.insert(track,curve)
   x = x + 1
   c = c + 1
  end
 end
end

-- begin program
createtrack()

while true do
 draw()
 os.sleep(0.1)
 cameraposition = cameraposition + 1 
 if cameraposition > 90000 then
  cameraposition = 0
 end
end