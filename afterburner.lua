--inspired by after burner

width, height = term.getSize()
horizon = 50 --percentage of the screen height
planeHeight = 0 --0 = middle
planePitch = 0 --value such that height += pitch 
planeAngle = 0 --up to 359
planeAngleD = 0 -- angle delta
planeEngine = 0 -- orange or red

function drawPlane()
  midpointx = width / 2
  midpointy = height / 2
  paintutils.drawLine(midpointx - 4, midpointy, midpointx + 6, midpointy, colors.gray)
  paintutils.drawLine(midpointx - 0, midpointy - 1, midpointx - 0, midpointy, colors.gray)  
  paintutils.drawLine(midpointx + 2, midpointy - 1, midpointx + 2, midpointy, colors.gray)
  if planeEngine == 0 then
    paintutils.drawPixel(midpointx - 0,midpointy,colors.red)
    paintutils.drawPixel(midpointx + 2,midpointy,colors.orange)
    planeEngine = 1
  else
    paintutils.drawPixel(midpointx - 0,midpointy,colors.orange)
    paintutils.drawPixel(midpointx + 2,midpointy,colors.red)
    planeEngine = 0
  end
end

function angleMod(xx)
  mod = 0 --positive lowers the horizon, negative raises
  middist = width / 2
  disttomid = math.abs(xx - middist)
  multiplier = disttomid / middist * height
  
  if xx < middist then
      mod = math.tan(math.rad(planeAngle)) * disttomid
    else -- xx >= middist
      mod = 0 - math.tan(math.rad(planeAngle)) * disttomid
  end
  return mod
end

function drawSkyWater()
  x = 1
  y = 1
  while y <= height do
    x = 1
    while x <= width do
      if planeAngle < 90 and planeAngle > -90 then
        if y <= horizon / 100 * height - planeHeight + angleMod(x) then
          paintutils.drawPixel(x,y,colors.lightBlue)
        else
          paintutils.drawPixel(x,y,colors.blue)
        end
      else -- plane upside down
        if y <= horizon / 100 * height - planeHeight + angleMod(x) then
          paintutils.drawPixel(x,y,colors.blue)
        else
          paintutils.drawPixel(x,y,colors.lightBlue)      
        end
      end   
      x = x + 1
    end
    y = y + 1
  end
  term.setTextColor(colors.yellow)
  term.setBackgroundColor(colors.lightBlue)
  term.setCursorPos(1,1)  
  write("height:  " .. planeHeight)
  term.setCursorPos(1,2)
  write("pitch:  " .. planePitch)  
  term.setCursorPos(40,1)
  write("roll:  " .. planeAngle)
  term.setCursorPos(20,2)
end

function Input()
   planePitch = planePitch + math.random(-2,2) / 10
   planeAngleD = math.random(-10,10) 
end

function Logic()
  if planePitch > 0.5 then
    planePitch = 0.5
  end
  if planePitch < -0.5 then
    planePitch = -0.5
  end
  planeHeight = planeHeight + planePitch

  if planeHeight > 4 then
    planeHeight = 4
  end
  if planeHeight < -4 then
    planeHeight = -4
  end
  
  planeAngle = planeAngle + planeAngleD
  if planeAngle < -180 then
   planeAngle = planeAngle + 360
  end
  if planeAngle > 180 then
    planeAngle = planeAngle - 360
  end
end

function Render()
  drawSkyWater()
  drawPlane()
end

while true do
  Input()
  Logic()
  Render()
  os.sleep(0.1)
end
