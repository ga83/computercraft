function polygon(xc,yc,radius,number,rotation)
 local n = number
 local c = 1
 local pointsx = {}
 local pointsy = {}
 local theta = rotation
 local x
 local y

 while c <= n do
  theta = math.rad((360 / n) * (c - 1) + rotation)
  y = math.sin(theta) * radius 
  x = math.cos(theta) * radius 
  table.insert(pointsx,x)
  table.insert(pointsy,y)
  
  c = c + 1
 end

 c = 1
 while c <= n  do
  if c == 1 then
   col = colors.red
  end
  if c == 2 then
   col = colors.orange
  end
  if c == 3 then
   col = colors.yellow
  end
  if c == 4 then
   col = colors.lime
  end
  if c == 5 then
   col = colors.blue
  end
  if c == 6 then
   col = colors.pink
  end
  if c == 7 then
   col = colors.white
  end
  if c == 8 then
   col = colors.grey
  end
  if c ~= n then
   paintutils.drawLine(pointsx[c] * 1.5 + xc ,pointsy[c] + yc ,pointsx[c + 1] * 1.5 + xc ,pointsy[c + 1] + yc ,col)
  else
   paintutils.drawLine(pointsx[c] * 1.5 + xc ,pointsy[c] + yc ,pointsx[1] * 1.5 + xc ,pointsy[1] + yc ,col)
  end
  c = c + 1
 end
end


function main()
 width,height = term.getSize()
 local rot = 0
 local crot = 0 -- rotation for centres
 local crotr1 -- crot in radians
 local crotr2
 local crotr3
 local crotr4
 local srot = 0
 local n = 4
 local rad = height / 2
 local x1,x2,x3,x4,y1,y2,y3,y4 
 local xc = width / 2
 local yc = height / 2
 local modecounter = 0 
 local rotspeed = 9.9
  
 while true do
  modecounter = modecounter + 1
  if modecounter == 2000 then
   modecounter = 0
   rotspeed = math.random() * 24 + 1
  end
  srot = srot + 0.7
  if srot >= 360 then
   srot = srot - 360
  end
  crot = crot + 0.4
  if crot >= 360 then
   crot = crot - 360
  end
  if crot <= 0 then
   crot = crot + 360
  end
  crotr1 = math.rad(crot)
  crotr2 = math.rad(crot + 90)
  crotr3 = math.rad(crot + 180)
  crotr4 = math.rad(crot + 270)  
  y1 = math.sin(crotr1) * rad
  y2 = math.sin(crotr2) * rad
  y3 = math.sin(crotr3) * rad
  y4 = math.sin(crotr4) * rad
  x1 = math.cos(crotr1) * rad * 1.5
  x2 = math.cos(crotr2) * rad * 1.5
  x3 = math.cos(crotr3) * rad * 1.5
  x4 = math.cos(crotr4) * rad * 1.5
    
    
  rot = rot + rotspeed
  if rot >= 360 then
   rot = rot - 360
  end
  if rot <= 0 then
   rot = rot + 360
  end
  if modecounter < 1000 then
   term.setBackgroundColor(colors.black)
   term.clear()
  end
  polygon(x1 + xc,y1 + yc,math.abs(180 - srot)  / 4 ,4,rot)
  polygon(x2 + xc,y2 + yc,math.abs(180 - srot ) / 4 ,5,rot)
  polygon(x3 + xc,y3 + yc,math.abs(180 - srot ) / 4 ,6,rot)
  polygon(x4 + xc,y4 + yc,math.abs(180 - srot ) / 4 ,7,rot)
  os.sleep(0.0)
 end
end


function main2()
 local rot = 0
 local rotr = math.rad(rot)
 width,height = term.getSize()
 local xc = width / 2
 local yc = height / 2
 while true do
  term.setBackgroundColor(colors.black)
  term.clear()
  polygon(xc,yc,height / 2 - 4,6,rot )
--  polygon(xc,yc,height / 2 - 3,192,rot)
  polygon(xc,yc,height / 2 - 1,6,rot )
  polygon(xc,yc,height / 2 - 7,6,rot )
  polygon(xc,yc,height / 2 - 10,6,rot)
  rot = rot + 3.7
  if rot >= 360 then
   rot = rot - 360
  end

  os.sleep(0.0)
 end
end



--main2()
main()