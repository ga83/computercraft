direction = 0 -- or 1
screenWidth,screenHeight = term.getSize()
position = screenWidth / 2
defaultgametime = 60
gametime = defaultgametime
framedelay = 0.1
obsx = {-1,-1,-1,-1,-1}
obsy = {-1,-1,-1,-1,-1}
hit = 0
score = 0
rsbc = 0

function updateObs()
 e = 1
 while e <= 5 do
  if obsx[e] == -1 then --so should obsy, no check
   if math.random(1,3) == 1 then
    obsx[e] = math.random(1,screenWidth)
    obsy[e] = 1
   end
  else
   if obsy[e] == screenHeight and obsx[e] == position then
    score = score - 200
    if rsbc <= 0 then
     redstone.setOutput("back",true)
     rsbc = 0.5
    end
   end
   obsy[e] = obsy[e] + 1
   if obsy[e] > screenHeight then
    obsy[e] = -1
    obsx[e] = -1
    score = score + 10
   end
  end
  e = e + 1
 end
 rsbc = rsbc - 0.1
 if rsbc <= 0.2 then
  redstone.setOutput("back",false)
 end
end

function drawObstacles()
 d = 1
 while d <= 5 do
  term.setCursorPos(obsx[d],obsy[d])
  term.setTextColor(colors.red)
  write("O")
  d = d + 1
 end
end --function

function updateScreen()
 term.clear()
 term.setCursorPos(1,1)
 term.setTextColor(colors.lightBlue)
 write("time: ")
 if gametime > 0 - framedelay and gametime < framedelay then
  gametime = 0
 end
 term.setTextColor(colors.lime)
 write(gametime)
 term.setTextColor(colors.lightBlue)
 write("\tscore: ")
 term.setTextColor(colors.lime)
 write(score)
 term.setCursorPos(1,screenHeight)
 c = 1
 while c < position do
  write(" ")
  c = c + 1
 end
 term.setTextColor(colors.yellow)
 write("A")
 drawObstacles()
end

function updateState()
 updateObs()
 gametime = gametime - framedelay
 position = position + direction
 if position < 1 then
  position = 1
 end
 if position > screenWidth then
  position = screenWidth
 end
end

function getInput()
 event, key = os.pullEvent()
 if event == "key" then
  if key == keys.a then
   direction = -1
  elseif key == keys.d then
   direction = 1
  end
  os.pullEvent("timer") --remove timer event from q
 else -- event is the timer
  direction = 0
 end

end

while true do
 os.startTimer(0)
 if gametime > 0 + framedelay then
  getInput()
  updateState()
  updateScreen()
  os.sleep(framedelay)
 end
 if gametime <= 0 + framedelay then
  term.setCursorPos(screenWidth / 2 - (string.len("GAME OVER") / 2),screenHeight / 2)
  term.setTextColor(colors.magenta)
  write("GAME OVER")
  obsx = {-1,-1,-1,-1,-1}
  obsy = {-1,-1,-1,-1,-1}
  if redstone.getInput("right") == true then
   gametime = defaultgametime
   score = 0
  end
 os.sleep(framedelay)
 end
end

