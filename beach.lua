
function draw(width,height,xs,xf,ywave,yhorizon)
 local x = 1
 local y = 1

 -- draw skyline
 while y < yhorizon do
  paintutils.drawLine(1,y,width,y,colors.orange)
  y = y + 1
 end
 
 -- draw ocean
 while y <= height do
  paintutils.drawLine(1,y,width,y,colors.blue)
  y = y + 1
 end
 
 -- draw wave
-- paintutils.drawLine(xs,ywave,xf,ywave,colors.white)
 local numparticles = 50
 local c = 0
 local xp
 local yp
 
 while c < numparticles do
  xp = xs + math.random() * (xf - xs)
  yp = ywave + math.random() * 2 - 1
  paintutils.drawPixel(xp,yp,colors.white)
  c = c + 1
 end


end

function main()
-- mon = peripheral.wrap('monitor_1')
-- term.redirect(mon) 
 local xs = 10
 local xf = 30
 local y = 0
 local speed
 local expand
 local width, height = term.getSize()
 local yh = height / 2
 local switch
 y = yh + 1
 speed = height / 20
 expand = width / 20

 while true do
  draw(width,height,xs,xf,y,yh) 
  y = y + speed
  xs = xs - expand
  xf = xf + expand
  if xs < 1 then
   xs = 0
  end
  if xf > width then
   xf = width
  end
  if y > height then
   y = yh + 1
   xs = math.random() * width
   xf = math.random() * width
   if xs > xf then
    switch = xs
    xs = xf
    xf = switch
   end
  end
  os.sleep(0.1)
 end
end

main()
